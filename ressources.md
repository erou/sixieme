## <i class="fa fa-compass" aria-hidden="true"></i> Ressources supplémentaires

Nous disposons d'un temps limité en classe, et vous trouverez peut-être parfois
que nous avons été vite sur une notion ou que nous n'avons pas fait assez
d'exercices.

Cette page vous donne quelques ressources, au cas où vous voudriez retravailler
le cours avec un support différent de celui vu en classe, ou si vous désirez
faire des exercices supplémentaires. N'ayez pas peur de faire des exercices non
corrigés : si vous ne trouvez pas la solution, je répondrai à vos questions avec
plaisir.

### <i class="fa fa-book" aria-hidden="true"></i> Des manuels

Ces manuels sont accessibles gratuitement en ligne.

+ [Le livre scolaire](https://www.lelivrescolaire.fr/manuels/mathematiques-6eme-2016)
+ [Sésamath](https://manuel.sesamath.net/numerique/?ouvrage=ms6_2013)

### <i class="fa fa-youtube-play" aria-hidden="true"></i> Chaînes YouTube

+ [M@ths et tiques](https://www.youtube.com/@YMONKA), la chaîne d'Yvan Monka,
  regroupe des milliers de vidéos sur toutes les mathématiques du collège et du
  lycée, c'est une mine d'or.
+ [EXERCICES MATHS](https://www.youtube.com/@videomaths), qui comme son nom
  l'indique propose de résoudre des exercices.
+ [J'ai 20 en maths](https://www.youtube.com/@Jai20enmaths), du cours et des
  exercices corrigés.
+ [Hans Amble - Maths au Lycée](https://www.youtube.com/@maths-lycee), également
  des cours et des résolutions d'exercices.
+ [KIFFELESMATHS](https://www.youtube.com/@KIFFELESMATHS), une autre chaîne proposant des
  cours et des exercices niveau lycée.
