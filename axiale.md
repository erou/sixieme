## Symétrie axiale

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/sixieme/pdfs/cours-symetrie-axiale.pdf)

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

Un cours complet sur la symétrie, pour revenir sur les points les moins
maîtrisés.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=ek0vhVpZOeh8WMBb&amp;list=PLVUDmbpupCarQBoHtQcjPvitHjCWDVGQ_" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
