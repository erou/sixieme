## Nombres entiers et décimaux

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/sixieme/pdfs/cours-nombres-entiers-decimaux.pdf)

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

Une série de vidéos sur YouTube permet de revenir sur toutes les notions
concernant les nombres entiers et décimaux.

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=u_U0thShFyTOBFQf&amp;list=PLVUDmbpupCaoCaqvyj8RGx07Ak0HSjOHi" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
