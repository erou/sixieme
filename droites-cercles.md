## Droites et cercles

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/sixieme/pdfs/cours-droites-cercles.pdf)
+ [Automatismes n°1](https://erou.forge.apps.education.fr/sixieme/pdfs/automatismes-1-droites-cercles.pdf)

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

<div style="text-align:center">
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?si=ISEbrHaCsTvaIXqp&amp;list=PLVUDmbpupCaoZtzpQWEJN1a-oQk8ZKFji" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>
