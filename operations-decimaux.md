## Opérations sur les nombres entiers et décimaux

### <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Fichiers de cours

+ [Cours](https://erou.forge.apps.education.fr/sixieme/pdfs/cours-operations-decimaux.pdf)

---

### <i class="fa fa-youtube-play" aria-hidden="true"></i> L'instant Monka

À venir.
